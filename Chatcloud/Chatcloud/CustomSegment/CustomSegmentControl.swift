//
//  FoodCustomSegmentControl.swift
//  EQ
//
//  Created by Mac on 11/03/2020.
//  Copyright © 2020 ITQ Plus. All rights reserved.
//

import Foundation
import UIKit
class CustomSegmentControl: UISegmentedControl {
    
    var selectedBackgroundColor = #colorLiteral(red: 0.231372549, green: 0.4745098039, blue: 1, alpha: 1)
    var sortedViews: [UIView]!
    var currentIndex: Int = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        sortedViews = self.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
        
        changeSelectedIndex(to: currentIndex)
        
        //self.tintColor.cgColor = #colorLiteral(red: 0.231372549, green: 0.4745098039, blue: 1, alpha: 1)
        self.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.9764705882, alpha: 1)
        
        self.layer.cornerRadius = 8
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        self.clipsToBounds = true
        
        let unselectedAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1) ,
                                    NSAttributedString.Key.font:  UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)] as [NSAttributedString.Key : Any]
        let selectedAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.1411764706, green: 0.2039215686, blue: 0.262745098, alpha: 1),
                                  NSAttributedString.Key.font:  UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)] as [NSAttributedString.Key : Any]
        self.setTitleTextAttributes(unselectedAttributes, for: .normal)
        self.setTitleTextAttributes(selectedAttributes, for: .selected)
    }
    
    func changeSelectedIndex(to newIndex: Int) {
        sortedViews[currentIndex].backgroundColor = UIColor.clear
        currentIndex = newIndex
        self.selectedSegmentIndex = UISegmentedControl.noSegment
        //sortedViews[currentIndex].backgroundColor = selectedBackgroundColor
    }
}
