//
//  CallHistory.swift
//  ChatCloud
//
//  Created by MACBOOK on 10/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit

class CallHistory: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let callarray = [
        ["img":"missedcall","time":"Missed Call","date":"05/10/2020"],
        ["img":"call","time":"35min","date":"10/09/2020"],
        ["img":"call","time":"20min","date":"25/09/2020"]]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return callarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CallHistory", for: indexPath)as! CallHistoryTableViewCell
        cell.callImage.image = UIImage(named: callarray[indexPath.row]["img"]!)
        cell.dateLabel.text = callarray[indexPath.row]["date"]!
        cell.timeLabel.text = callarray[indexPath.row]["time"]!
        return cell
     }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    

}
