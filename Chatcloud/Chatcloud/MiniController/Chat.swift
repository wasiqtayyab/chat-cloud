//
//  Chat.swift
//  ChatCloud
//
//  Created by MACBOOK on 09/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit

class Chat: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView0: UITableView!
    
    
    let chatarray = [
        ["name":"Annette Black","img":"profile1","message":"Hello how are you","time":"2min ago"],
        ["name":"Hey guuurll","img":"profile2","message":"sfjgsakfjdhgafjkgasfdjgasfkjgaskfjakfgaskfjgkajshfgkjasfkasfkjafkagfk","time":"5min ago"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    //MARK:- TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatList", for: indexPath) as! ChatViewCell
        cell.profileName.text = chatarray[indexPath.row]["name"]!
        cell.profileImage.image = UIImage(named: chatarray[indexPath.row]["img"]!)
        cell.messageLabel.text = chatarray[indexPath.row]["message"]!
        cell.timeLabel.text = chatarray[indexPath.row]["time"]!
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    
}
