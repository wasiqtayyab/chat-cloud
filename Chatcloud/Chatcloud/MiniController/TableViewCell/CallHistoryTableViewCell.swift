//
//  CallHistoryTableViewCell.swift
//  ChatCloud
//
//  Created by MACBOOK on 10/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit

class CallHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var callImage: UIImageView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
