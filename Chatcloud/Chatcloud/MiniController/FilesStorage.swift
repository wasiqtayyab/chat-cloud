//
//  FilesStorage.swift
//  ChatCloud
//
//  Created by MACBOOK on 12/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit

class FilesStorage: UIViewController
{
   
    //MARK:- array for file

    let filesarray = ["Square","Square-1","Square-2","Square-3","Square-4","Square-5","Square-6"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}


//MARK:- CollectionView
extension FilesStorage: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filesarray.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilesStorage", for: indexPath)as! FilesStorageCollectionViewCell
        
        cell.fileImage.image = UIImage(named: filesarray[indexPath.row])
        return cell
       }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
      return CGSize(width: collectionView.frame.width / 3 , height: 100)
    }

    
}
