//
//  MainScreen.swift
//  ChatCloud
//
//  Created by MACBOOK on 09/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit

class MainScreen: UIViewController {
    
    var dis = ""
    var name = ""
    var image  :  UIImage!
    var statusimg : UIImage!
    var statuslbl = ""
    //MARK:- OUTLET
   
    
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var discLabel: UILabel!
    
    ///container view
    
    @IBOutlet weak var chatContainerView: UIView!
    @IBOutlet weak var callContainerView: UIView!
    @IBOutlet weak var filesContainerView: UIView!
    ///button
    
    
    @IBOutlet weak var chatButton: UIButton!
    
    @IBOutlet weak var callButton: UIButton!
    
    @IBOutlet weak var filesButton: UIButton!
    
    
    ///line view
    @IBOutlet weak var chatlineView: UIView!
    @IBOutlet weak var calllineView: UIView!
    @IBOutlet weak var filelineView: UIView!
    
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(statuslbl)
        self.profileButton.setImage(image, for: .normal)
        nameLabel.text = name
        discLabel.text = dis
        
        profileButton.layer.cornerRadius = profileButton.frame.size.width / 2
        profileButton.clipsToBounds = true
        profileButton.imageView?.contentMode = .scaleAspectFill
        
        ///bottom border
        let bottomBorder = CALayer()
        bottomBorder.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1);
        bottomBorder.borderWidth = 1;
        bottomBorder.frame = CGRect(x: 0, y: buttonView.frame.height, width: view.frame.width, height: 2)
        buttonView.layer.addSublayer(bottomBorder)
        
        callContainerView.isHidden = true
        calllineView.isHidden = true
        filelineView.isHidden = true
        filesContainerView.isHidden = true
       
    }
        //MARK:- Action
    
    @IBAction func chatButtonPressed(_ sender: UIButton) {
        
        callContainerView.isHidden = true
        calllineView.isHidden = true
        callButton.setTitleColor(#colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1), for: .normal)
        
        
        filesButton.setTitleColor(#colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1), for: .normal)
        filesContainerView.isHidden = true
        filelineView.isHidden = true
        
        
        chatlineView.isHidden = false
        chatContainerView.isHidden = false
        chatButton.setTitleColor(#colorLiteral(red: 0.2156862745, green: 0.4901960784, blue: 1, alpha: 1), for: .normal)
        
    }
    
    
    @IBAction func callButtonPressed(_ sender: UIButton) {
        
        chatContainerView.isHidden = true
        chatlineView.isHidden = true
        chatButton.setTitleColor(#colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1), for: .normal)
        
        
        filesButton.setTitleColor(#colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1), for: .normal)
        filesContainerView.isHidden = true
        filelineView.isHidden = true
        
        
        calllineView.isHidden = false
        callContainerView.isHidden = false
        callButton.setTitleColor(#colorLiteral(red: 0.2156862745, green: 0.4901960784, blue: 1, alpha: 1), for: .normal)
        
    }
    
    @IBAction func fileButtonPressed(_ sender: UIButton) {
        
        callContainerView.isHidden = true
        calllineView.isHidden = true
        callButton.setTitleColor(#colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1), for: .normal)
        
        
        chatButton.setTitleColor(#colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1), for: .normal)
        chatlineView.isHidden = true
        chatContainerView.isHidden = true
        
        
        filelineView.isHidden = false
        filesContainerView.isHidden = false
        filesButton.setTitleColor(#colorLiteral(red: 0.2156862745, green: 0.4901960784, blue: 1, alpha: 1), for: .normal)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "UsersInboxVC")as! UsersInbox
        vc.userName = self.name
        vc.discription = self.dis
        vc.status = self.statusimg
        vc.img = self.image
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func profileButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileScreen")as! EditProfileScreen
        
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}
