//
//  ChatPeople.swift
//  ChatCloud
//
//  Created by MACBOOK on 07/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit

class ChatPeople: UIViewController,UITableViewDelegate,UITableViewDataSource{
   
    //MARK:- Array decleration

    let userarray = [
           ["name":"Annette Black","status":"at work","img":"useractive"],
           ["name":"Brennda Smily","status":"Busy","img":"userbusy"],
           ["name":"Cameron Williamson","status":"at work","img":"useractive1"],
           ["name":"Esther Howard","status":"at work","img":"useractive2"]]
           
       
       

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
   
    
    //MARK:- TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserName", for: indexPath) as! chatTableViewCell
        
        cell.profileImage.image = UIImage(named: userarray[indexPath.row]["img"]!)
        cell.nameLabel.text = userarray[indexPath.row]["name"]!
        cell.statusLabel.text = userarray[indexPath.row]["status"]!
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    

}
