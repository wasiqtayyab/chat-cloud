//
//  GroupChat.swift
//  ChatCloud
//
//  Created by MACBOOK on 08/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit

class GroupChat: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableView1: UITableView!
    //MARK:- Array decleration

    let grouparray = [
        ["name":"Annette Black","stat":"at work","img0":"useractive"],
        ["name":"Brennda Smily","stat":"Busy","img0":"userbusy"],
        ["name":"Cameron Williamson","stat":"at work","img0":"useractive1"],
        ["name":"Esther Howard","stat":"at work","img0":"useractive2"]]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return grouparray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupChat", for: indexPath) as! groupchatTableViewCell
        
        cell.profileImage.image = UIImage(named: grouparray[indexPath.row]["img0"]!)
        cell.profileName.text = grouparray[indexPath.row]["name"]!
        cell.workStatus.text = grouparray[indexPath.row]["stat"]!
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
   

}
