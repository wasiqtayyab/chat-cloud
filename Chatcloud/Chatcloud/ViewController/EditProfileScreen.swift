//
//  EditProfileScreen.swift
//  ChatCloud
//
//  Created by MACBOOK on 12/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit
import Firebase

class EditProfileScreen: UIViewController,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var ref: DatabaseReference!

   
    //MARK:- OUTLET
    
    @IBOutlet weak var imagePicker: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var dropdownView: UIView!
    @IBOutlet weak var tableViewheight: NSLayoutConstraint!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var statusview: UIView!
    @IBOutlet weak var dropdownButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    @IBOutlet weak var discTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    
    
    //MARK:- array for file
    
    let statusarray = [["img":"Circle","status":"Available"],["img":"Circle1","status":"Busy"],
    ["img":"Circle3","status":"Offline"]]
    

    let imagearray = ["Square-7","Square-8","Square-9","Square-10","Square-11","Square-12"]

   
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
       ref = Database.database().reference()
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
    
        profileImage.contentMode = .scaleAspectFill

        doneButton.isHidden = true
        tableView1.isHidden = true
        dropdownView.layer.cornerRadius = 4
        dropdownView.layer.borderWidth = 1
        dropdownView.layer.borderColor = #colorLiteral(red: 0.8078431373, green: 0.8078431373, blue: 0.8078431373, alpha: 1)
    }
    
    //MARK:- IMAGE PICKER BUTTON
       
       @IBAction func imagePickerPressed(_ sender: UIButton) {
           
           let imagePickerController = UIImagePickerController()
           imagePickerController.delegate = self
           
           let actionSheet = UIAlertController()
           actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction) in
               imagePickerController.sourceType = .camera
               self.present(imagePickerController,animated: true,completion: nil)
           }))
           actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
               self.present(imagePickerController,animated: true,completion: nil)
           }))
           
           actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
               
           self.present(actionSheet,animated: true,completion: nil)
       }
       
       
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
           let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
           profileImage.image = image
           picker.dismiss(animated: true, completion: nil)
           
       }
       
       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           picker.dismiss(animated: true, completion: nil)
       }
       
    
    //MARK:- ACTION
    
    @IBAction func doneButtonPressed(_ sender: UIButton) {
        
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainScreen")as! MainScreen
        vc.name = nameTF.text!
        vc.dis = discTF.text!
        vc.image =  self.profileImage.image
        vc.statusimg = statusImage.image
        vc.statuslbl = statusLabel.text!
        
                navigationController?.pushViewController(vc, animated: true)
        let uderid = (Auth.auth().currentUser?.uid)!
        ref.child("users").child(uderid).updateChildValues(["name": vc.name])
        
        
    }
    
    @IBAction func nameTextField(_ sender: UITextField) {
        doneButton.isHidden = false
       self.doneButton.setTitleColor(#colorLiteral(red: 0.231372549, green: 0.4549019608, blue: 0.8274509804, alpha: 1), for: .normal)
        
    }
    
    @IBAction func dropdownButtonPressed(_ sender: UIButton) {
        if tableView1.isHidden{
            animate(toogle: true)
        }else {
            animate(toogle: false)
        }
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- DROPDOWN Animation
        
        func animate(toogle:Bool){
            if toogle{
                UIView.animate(withDuration: 0.3){
                    self.tableView1.isHidden = false
                    self.tableViewheight.constant =  45 * 3
                }
            }else {
                UIView.animate(withDuration: 0.3){
                    self.tableView1.isHidden = true
                      self.tableViewheight.constant =  100
                }
            }
        }
        
    }
    
    

    //MARK:- CollectionView
    extension EditProfileScreen: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
        
        
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return imagearray.count
           }
           
           func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditProfile", for: indexPath)as! EditProfileCollectionViewCell
            
            cell.imageButton.setImage(UIImage(named: imagearray[indexPath.row]), for: .normal)
            cell.imageButton.imageView?.contentMode = .scaleAspectFill
            cell.imageButton.tag =  indexPath.row
            cell.imageButton.addTarget(self, action: #selector(self.AddImage(btn:)), for: .touchUpInside)
            return cell
           }
        
        
        @objc func AddImage(btn:UIButton){
            //            print(self.imagearray[btn.tag])
            self.profileImage.image = UIImage(named:self.imagearray[btn.tag])
        }
        
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
          return CGSize(width: collectionView.frame.width / 3 , height: 100)
        }
        

}

//MARK:- TABLEVIEW

extension EditProfileScreen: UITableViewDelegate,UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profilescreen", for: indexPath)as! EditScreenTableViewCell
        cell.statusImage.image = UIImage(named: statusarray[indexPath.row]["img"]!)
        cell.statusLabel.text =
            statusarray[indexPath.row]["status"]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.statusLabel.text = (statusarray[indexPath.row]["status"]!)

        self.statusImage.image = UIImage(named: (statusarray[indexPath.row]["img"]!))
      
        animate(toogle: false)
    }
    
}

