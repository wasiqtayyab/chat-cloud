//
//  SignupScreen.swift
//  ChatCloud
//
//  Created by WASIQ-MACBOOK on 02/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseUI
import AuthenticationServices
import CryptoKit


class SignupScreen: UIViewController,GIDSignInDelegate, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding{
    
    var ref: DatabaseReference!

    // Unhashed nonce.
    fileprivate var currentNonce: String?
   //MARK:- OUTLETS
   @IBOutlet weak var appleButton: UIButton!
   @IBOutlet weak var phoneButton: UIButton!
   @IBOutlet weak var facebookButton: UIButton!
   @IBOutlet weak var googleButton: UIButton!
    
    
    
    @IBOutlet weak var heightAppleButton: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ref = Database.database().reference()

        // MARK:- Add a corner radius
        //for google button
        googleButton.layer.cornerRadius = 8
        googleButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        ///add a shadow
        googleButton.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
        googleButton.layer.shadowOpacity = 1
        googleButton.layer.shadowOffset = .zero
        googleButton.layer.shadowRadius = 2
        
        //for facebook button
        facebookButton.layer.cornerRadius = 8
        facebookButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        ///add a shadow
        facebookButton.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
        facebookButton.layer.shadowOpacity = 1
        facebookButton.layer.shadowOffset = .zero
        facebookButton.layer.shadowRadius = 2
        

        //for apple button
        appleButton.layer.cornerRadius = 8
        appleButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        ///add a shadow
        appleButton.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
        appleButton.layer.shadowOpacity = 1
        appleButton.layer.shadowOffset = .zero
        appleButton.layer.shadowRadius = 2
        
        //for phone button
        phoneButton.layer.cornerRadius = 8
        phoneButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        ///add a shadow
        phoneButton.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1)
        phoneButton.layer.shadowOpacity = 1
        phoneButton.layer.shadowOffset = .zero
        phoneButton.layer.shadowRadius = 2
        
         GIDSignIn.sharedInstance().delegate = self
        
        if #available(iOS 13, *){
            self.heightAppleButton.constant = 60
            self.appleButton.isHidden = false
        }else{
            self.heightAppleButton.constant = 0
                       self.appleButton.isHidden = true
        }
        
        
    }
    
   
    //MARK:- GOOGLE
    var imgurl = ""
    var name = ""
    var email = ""
    var token = ""
    var uid = ""
    
    
    @IBAction func googleButtonPressed(_ sender: UIButton) {
        
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print("\(error.localizedDescription)")
         return
       }

       guard let authentication = user.authentication else { return }
       let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,accessToken: authentication.accessToken)
        
        Auth.auth().signIn(with: credential,completion: { (authResult, error) in
            self.uid = (authResult?.user.uid)!
            self.name = (authResult?.user.displayName)!
            self.email = (authResult?.user.email)!
            self.imgurl = (authResult?.user.photoURL?.absoluteString)!
            
            /*self.ref.child("users/\(useruid!)/email").setValue(self.email)
            r
            self.ref.child("users/\(useruid!)/imgurl").setValue(self.imgurl)*/
            var param = [String:Any]()
            param = [
                "name": self.name,
                "email": self.email,
                "imgurl": self.imgurl,
            ]
            
            if(error != nil){
                print("\(error!.localizedDescription)")
                return
            }else {
                registerUserInDatabaseWithURl(self.uid, value: param)
            print("Sign-in Successfull")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile")
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        })
        
        
        func registerUserInDatabaseWithURl(_ uid:String , value:[String:Any]){
               let reference = Database.database().reference()
               let user = reference.child("users").child(uid)
               let userId = Auth.auth().currentUser!.uid
               user.updateChildValues(value) {(error, ref) in
                   if error != nil{
                       print(error ?? "")
                       return
                   }
                   print("user Registered")
               }
               
           }
        
        
       
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("User has disconnected from this process")
        }
    
     //MARK:-FACEBOOK
    
    @IBAction func facebookButtonPressed(_ sender: UIButton) {
    
       let loginManager = LoginManager()
          loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
              if let error = error {
                  print("Failed to login: \(error.localizedDescription)")
                  return
              }
              
              guard let accessToken = AccessToken.current else {
                  print("Failed to get access token")
                  return
              }

              let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
              
              // Perform login by calling Firebase APIs
              Auth.auth().signIn(with: credential, completion: { (user, error) in
                  if let error = error {
                      print("Login error: \(error.localizedDescription)")
                      let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                      let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                      alertController.addAction(okayAction)
                      self.present(alertController, animated: true, completion: nil)
                      return
                  }
                self.uid = (user?.user.uid)!
                self.getFBUserData()
              })

          }
      
    
    }
    
    func getFBUserData(){
        
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,age_range"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    //print(dict)
                    if let dict = result as? [String : AnyObject]{
                        if(dict["email"] as? String == nil || dict["id"] as? String == nil || dict["email"] as? String == "" || dict["id"] as? String == "" ){
                            
                        }else{
                            self.email = (dict["email"] as? String)!
                            self.name = (dict["name"] as? String)!
                            let dic1 = dict["picture"] as! NSDictionary
                            let pic = dic1["data"] as! NSDictionary
                            self.imgurl = (pic["url"] as? String)!
                            
                            var param = [String:Any]()
                            param = [
                                "name": self.name,
                                "email": self.email,
                                "imgurl": self.imgurl,
                            ]
                            
                            self.registerUserInDatabaseWithURl(self.uid, value: param)

                             
                        }
                    }
                    
                }
            })
        }
        
    }
    func registerUserInDatabaseWithURl(_ uid:String , value:[String:Any]){
                  let reference = Database.database().reference()
                  let user = reference.child("users").child(uid)
                  let userId = Auth.auth().currentUser!.uid
                  user.updateChildValues(value) {(error, ref) in
                      if error != nil{
                          print(error ?? "")
                          return
                      }
                      print("User Registered")
                  }
                  
              }

    //MARK:-APPLE
   @available(iOS 13, *)
     @IBAction func appleButtonPressed(_ sender: UIButton) {
    let appleIDProvider = ASAuthorizationAppleIDProvider()
    let request = appleIDProvider.createRequest()
    request.requestedScopes = [.fullName, .email]

    // Generate nonce for validation after authentication successful
    self.currentNonce = randomNonceString()
    // Set the SHA256 hashed nonce to ASAuthorizationAppleIDRequest
    request.nonce = sha256(currentNonce!)

    // Present Apple authorization form
    let authorizationController = ASAuthorizationController(authorizationRequests: [request])
    authorizationController.delegate = self
    authorizationController.presentationContextProvider = self
    authorizationController.performRequests()
    
   
     }
     
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
@available(iOS 13.0, *)
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            // Save authorised user ID for future reference
            UserDefaults.standard.set(appleIDCredential.user, forKey: "appleAuthorizedUserIdKey")
            
            // Retrieve the secure nonce generated during Apple sign in
            guard let nonce = self.currentNonce else {
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }

            // Retrieve Apple identity token
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Failed to fetch identity token")
                return
            }

            // Convert Apple identity token to string
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Failed to decode identity token")
                return
            }

            // Initialize a Firebase credential using secure nonce and Apple identity token
            let firebaseCredential = OAuthProvider.credential(withProviderID: "apple.com",
                                                              idToken: idTokenString,
                                                              rawNonce: nonce)
                
            // Sign in with Firebase
            Auth.auth().signIn(with: firebaseCredential) { (authResult, error) in
                
                if let error = error {
                    print(error.localizedDescription)
                    return
                }else {
                    //let changeRequest = authResult?.user.createProfileChangeRequest()
                    self.name = (appleIDCredential.fullName?.givenName)!
                    self.uid = (authResult?.user.uid)!
                    self.email = (authResult?.user.email)!
                    
                    var param = [String:Any]()
                    param = [
                        "name": self.name,
                        "email": self.email
                    ]
                    
                    self.registerUserInDatabaseWithURl(self.uid, value: param)
                
                }
            
            }
            
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("authorizationController Error: \(error.localizedDescription)")
    }
    @available(iOS 13.0, *)
   func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
    return view.window!.self
   }
    
    //MARK:- CONTINUE WITH PHONE NUMBER
    
    @IBAction func phoneButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "PhoneNumberController") as! PhoneNumberController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func signinButton(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
               
               self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    

}
    
    
    
    
    
    
    
    
    


   
