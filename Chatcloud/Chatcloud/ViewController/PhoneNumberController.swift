//
//  PhoneNumberController.swift
//  ChatCloud
//
//  Created by WASIQ-MACBOOK on 30/09/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit
import PhoneNumberKit
import ContactsUI
import Foundation
import FirebaseAuth
import FirebaseCore


class PhoneNumberController: UIViewController,CNContactPickerDelegate{
    
    var currentVerificationId = ""
    var signup = true
    var ischecked = false
    //MARK:- OUTLETS
    
    @IBOutlet weak var signhereButton: UIButton!
    @IBOutlet weak var conditionView: UIView!
    @IBOutlet weak var checkedButton: UIButton!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var phoneTextField: PhoneNumberTextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if signup == true {
            
            signinButton.setTitle("Sign Up", for: .normal)
            signhereButton.setTitle("Sign in here", for: .normal)
            
        }else{
            
            conditionView.isHidden = true
            
        }
        
        // MARK:- Add a corner radius
        
        signinButton.layer.cornerRadius = 8
        signinButton.layer.borderColor = #colorLiteral(red: 0.2156862745, green: 0.4901960784, blue: 1, alpha: 1)
        
        
        checkedButton.layer.borderWidth = 1
        checkedButton.layer.cornerRadius = 2
        checkedButton.layer.borderColor = #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1)
        
        errorLabel.isHidden = true
        
    }
    
    //MARK:- ACTION
    
    @IBAction func conditionButtonPressed(_ sender: UIButton) {
        
        guard let url = URL(string: "https://getvibezapp.com/terms.html") else { return }
        UIApplication.shared.open(url)
        
    }
    
    
    
    
    @IBAction func termButtonPressed(_ sender: UIButton) {
        
        if ischecked == false{
            checkedButton.setImage(UIImage(named: "tick"), for: .normal)
            self.ischecked = true
        }else {
            checkedButton.setImage(UIImage(named: "unchecked"), for: .normal)
            self.ischecked = false
        }
        
    }
    
    
    
    //SIGN UP SCREEN BUTTON

    @IBAction func signButtonPressed(_ sender: UIButton) {
        
        if self.signup == true{
            let vc = storyboard?.instantiateViewController(withIdentifier:"LoginScreen" ) as! LoginScreen
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "SignupScreen") as! SignupScreen
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
    
    
    
    //BACK BUTTON

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //SIGN IN BUTTON
    @IBAction func signinButton(_ sender: UIButton) {
        
        if signup == true {
            if ischecked == true{
                errorLabel.isHidden = true
                //MARK:-PHONE AUTHETICATION
                
                // Step 3 (Optional) - Default language is English
                Auth.auth().languageCode = "en"
                let phoneNumber = self.phoneTextField.text!
                print(phoneNumber)
                // Step 4: Request SMS
                PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                    if let error = error {
                        print(error.localizedDescription)
                        
                    }else {
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPScreen") as! OTPScreen
                        vc.VerifcationCode = verificationID!
                        vc.phonenumber = phoneNumber
                        self.navigationController?.pushViewController(vc, animated: true)
                        //    self.present(vc, animated: true, completion: nil)
                    }
                }
            }else {
                errorLabel.isHidden = false
                self.errorLabel.text = "Please check the terms & condition"
                
            }
            
        }else {
            
            errorLabel.isHidden = true
            
            // Step 3 (Optional) - Default language is English
            Auth.auth().languageCode = "en"
            let phoneNumber = self.phoneTextField.text!
            print(phoneNumber)
            // Step 4: Request SMS
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                if let error = error {
                    print(error.localizedDescription)
                    
                }else {
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPScreen") as! OTPScreen
                    vc.signup = self.signup
                    vc.VerifcationCode = verificationID!
                    vc.phonenumber = phoneNumber
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
            }
            
        }
        
        
    }
}


