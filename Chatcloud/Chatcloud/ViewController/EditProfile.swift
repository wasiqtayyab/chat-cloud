//
//  EditProfile.swift
//  ChatCloud
//
//  Created by WASIQ-MACBOOK on 02/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SDWebImage

class EditProfile: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
    var ref: DatabaseReference!
   
    //MARK:-ARRAY
    
    let statusarray = [["img":"Circle","status":"Available"],["img":"Circle1","status":"Busy"],
    ["img":"Circle3","status":"Offline"]]
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tabelviewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var discTF: UITextField!
    @IBOutlet weak var completeButton: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var imgStatus: UIImageView!
    
    
    //MARK:- VIEW DID LOAD
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        
        tableView1.isHidden = true
        
        completeButton.layer.cornerRadius = 8
        completeButton.layer.borderColor = #colorLiteral(red: 0.2156862745, green: 0.4901960784, blue: 1, alpha: 1)
        
        statusView.layer.cornerRadius = 4
        statusView.layer.borderWidth = 1
        statusView.layer.borderColor = #colorLiteral(red: 0.8078431373, green: 0.8078431373, blue: 0.8078431373, alpha: 1)
        
        errorLabel.isHidden = true
        ref = Database.database().reference()

        firebaseFetchData()
        
     
    }
    
    func firebaseFetchData(){
        let userID = (Auth.auth().currentUser?.uid)!
        print(userID)
        ref.child("users").child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
          // Get user value
          let value = snapshot.value as? NSDictionary
            let name = value?["name"] as? String ?? ""
            let imageurl = value?["imgurl"] as? String ?? ""
            print(name)
            self.nameTF.text = name
           // self.profileImage.image = UIImage(named: imageurl)
             self.profileImage.sd_setImage(with: URL(string: imageurl), placeholderImage: UIImage(named: "imagepreview"))

          // ...
          }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    //MARK:- IMAGE PICKER BUTTON
    
    @IBAction func imagePickerPressed(_ sender: UIButton) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController()
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            self.present(imagePickerController,animated: true,completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
        self.present(actionSheet,animated: true,completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profileImage.image = image
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)

    }
    
    
    @IBAction func completeButtonPressed(_ sender: UIButton) {
        if let text = nameTF.text,text.isEmpty{
            
            errorLabel.isHidden = false
            
        }else if let text1 = discTF.text,text1.isEmpty{
            
            errorLabel.isHidden = false
            
        }else {
            errorLabel.isHidden = true
            let vc = storyboard?.instantiateViewController(withIdentifier: "UsersInboxVC")as! UsersInbox
                vc.userName = self.nameTF.text!
            vc.discription = self.discTF.text!

            
            vc.img = self.profileImage.image
            vc.status = self.imgStatus.image
            navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    
    @IBAction func statusButtonPressed(_ sender: UIButton) {
        
        if tableView1.isHidden{
            animate(toogle: true)
        }else {
            animate(toogle: false)
        }
    }
    
    //MARK:- DROPDOWN Animation
    
    func animate(toogle:Bool){
        if toogle{
            UIView.animate(withDuration: 0.3){
                self.tableView1.isHidden = false
                self.tabelviewHeight.constant =  45 * 3
            }
        }else {
            UIView.animate(withDuration: 0.3){
                self.tableView1.isHidden = true
                  self.tabelviewHeight.constant =  100
            }
        }
    }
    
}

//MARK:- TABLEVIEW


extension EditProfile: UITableViewDelegate,UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfile", for: indexPath)as! EditProfileTableViewCell
        cell.statusImage.image = UIImage(named: statusarray[indexPath.row]["img"]!)
        cell.statusLabel.text =
            statusarray[indexPath.row]["status"]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.lblStatus.text = (statusarray[indexPath.row]["status"]!)

        self.imgStatus.image = UIImage(named: (statusarray[indexPath.row]["img"]!))
      
        animate(toogle: false)
    }
    
}
