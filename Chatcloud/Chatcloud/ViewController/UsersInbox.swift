//
//  UsersInbox.swift
//  ChatCloud
//
//  Created by WASIQ-MACBOOK on 03/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit


class UsersInbox: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var userName = ""
    var discription = ""
    var img : UIImage!
    var status : UIImage!
    //MARK:- OUTLET
    
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var mySegmentControl: CustomSegmentControl!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var profileImg: UIButton!
    @IBOutlet weak var statusImg: UIImageView!
    
    //MARK:- Array decleration
    let name = [
        ["name":"Annette Black","img":"profile1","time":"2min ago","message":"ghfhfhgfghfghfghfghfghghghghghghfghghghgfghghfghgfghghghghghghghhghgghgh"],
        ["name":"Hey guuurll","img":"profile2","time":"5min ago","message":"Sent a picture"],
        ["name":"Cameron Williamson","img":"profile3","time":"10min ago","message":"How are you"],
        ["name":"Jane Cooper","img":"profile4","time":"15min ago","message":"What is your favourite dish"]]
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mySegmentControl.selectedSegmentIndex = 0
        self.mySegmentControl.selectedBackgroundColor = .blue
        
        
        statusImg.image = status
        
        profileImg.layer.cornerRadius = profileImg.frame.size.width / 2
        profileImg.clipsToBounds = true
        profileImg.setImage(img, for: .normal)
        profileImg.imageView?.contentMode = .scaleAspectFill

    }
    
    //MARK:- TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "usersTVC", for: indexPath) as! usersTableViewCell
        cell.profileImage.image = (UIImage(named: name[indexPath.row]["img"]!))
        cell.userName.text = (name[indexPath.row]["name"]!)
         cell.messageLabel.text = name[indexPath.row]["message"]!
         cell.minuteLabel.text = name[indexPath.row]["time"]!
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK:- Action
    
    @IBAction func chatButtonPressed(_ sender: UIButton) {
       
    }
    
    @IBAction func profileButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainScreen")as! MainScreen
        vc.image = self.img
        vc.dis = self.discription
        vc.name = self.userName
        vc.statusimg = self.statusImg.image
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
    


    

