//
//  usersTableViewCell.swift
//  ChatCloud
//
//  Created by MACBOOK on 06/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit

class usersTableViewCell: UITableViewCell {

    //MARK:- OUTLET
    

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var tableView0: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
