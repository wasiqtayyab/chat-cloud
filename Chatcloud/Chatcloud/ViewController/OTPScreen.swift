//
//  OTPScreen.swift
//  ChatCloud
//
//  Created by WASIQ-MACBOOK on 01/10/2020.
//  Copyright © 2020 MACBOOK PRO. All rights reserved.
//

import UIKit
import FirebaseAuth


class OTPScreen: UIViewController ,UITextFieldDelegate{
    
    var phonenumber = ""
    var signup = true
    var VerifcationCode = ""
    
    //MARK: - OUTLET
    @IBOutlet weak var resendcodeButton: UIButton!
    @IBOutlet weak var otp0TextField: UITextField!
    @IBOutlet weak var otp1TextField: UITextField!
    @IBOutlet weak var otp2TextField: UITextField!
    @IBOutlet weak var otp3TextField: UITextField!
    @IBOutlet weak var otp4TextField: UITextField!
    @IBOutlet weak var otp5TextField: UITextField!
    
    @IBOutlet weak var signinButton: UIButton!
    
    @IBOutlet weak var codeLabel: UILabel!
    
    var strtOTP = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("VerifcationCode",VerifcationCode)
        print(signup)
        if signup == true{
            
            signinButton.setTitle("Sign Up", for: .normal)
        }else {
            
            //signinButton.setTitle("Sign In", for: .normal)
         /*   let vc = storyboard?.instantiateViewController(withIdentifier: "MainScreen")as! MainScreen
            navigationController?.pushViewController(vc, animated: true)*/
            
        }
        
        //MARK:- Add a corner radius
        
        signinButton.layer.cornerRadius = 8
        signinButton.layer.borderColor = #colorLiteral(red: 0.2156862745, green: 0.4901960784, blue: 1, alpha: 1)
        
        //MARK:- add a border color
        
        otp0TextField.layer.cornerRadius = 8
        otp0TextField.layer.borderWidth = 1
        otp0TextField.layer.borderColor = #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1)
        
        otp1TextField.layer.cornerRadius = 8
        otp1TextField.layer.borderWidth = 1
        otp1TextField.layer.borderColor = #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1)
        
        
        otp2TextField.layer.cornerRadius = 8
        otp2TextField.layer.borderWidth = 1
        otp2TextField.layer.borderColor = #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1)
        
        
        otp3TextField.layer.cornerRadius = 8
        otp3TextField.layer.borderWidth = 1
        otp3TextField.layer.borderColor = #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1)
        
        otp4TextField.layer.cornerRadius = 8
        otp4TextField.layer.borderWidth = 1
        otp4TextField.layer.borderColor = #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1)
        
        otp5TextField.layer.cornerRadius = 8
        otp5TextField.layer.borderWidth = 1
        otp5TextField.layer.borderColor = #colorLiteral(red: 0.6666666667, green: 0.6901960784, blue: 0.7176470588, alpha: 1)
        
        codeLabel.isHidden = true
        //Delegate
        otp0TextField.delegate = self
        otp1TextField.delegate = self
        otp2TextField.delegate = self
        otp3TextField.delegate = self
        otp4TextField.delegate = self
        otp5TextField.delegate = self
        otp0TextField.becomeFirstResponder()
    }
    
    
    
   
    
    
    
    
    //MARK:- TEXTFIELD OTP CODE
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text!.count < 1) && (string.count > 0){
            
            if textField == otp0TextField{
                
                otp1TextField.becomeFirstResponder()
                
            }
            if textField == otp1TextField{
                
                otp2TextField.becomeFirstResponder()
                
            }
            if textField == otp2TextField{
                
                otp3TextField.becomeFirstResponder()
                
            }
            if textField == otp3TextField{
                
                otp4TextField.becomeFirstResponder()
                
            }
            if textField == otp4TextField{
                otp5TextField.becomeFirstResponder()
                
            }
            if textField == otp5TextField{
                otp5TextField.resignFirstResponder()
                
            }
            
            textField.text = string
            let codeField = "\(self.otp0TextField.text!)\(otp1TextField.text!)\(otp2TextField.text!)\(otp3TextField.text!)\(otp4TextField.text!)\(otp5TextField.text!)"
            self.strtOTP = codeField
            print(strtOTP)
            return false
        }else
            //for reverse order in textfield
            if (textField.text!.count >= 1) && (string.count == 0){
                
                if textField == otp1TextField {
                    otp0TextField.becomeFirstResponder()
                }
                if textField == otp2TextField {
                    otp1TextField.becomeFirstResponder()
                }
                if textField == otp3TextField {
                    otp2TextField.becomeFirstResponder()
                }
                
                if textField == otp5TextField{
                    otp4TextField.becomeFirstResponder()
                }
                if textField == otp4TextField{
                    otp3TextField.becomeFirstResponder()
                }
                if textField == otp0TextField {
                    otp0TextField.resignFirstResponder()
                }
                
                textField.text = string
                return false
            } else if (textField.text!.count >= 1) {
                
                textField.text = string
                return false
        }
        let codeField = "\(self.otp0TextField.text!)\(otp1TextField.text!)\(otp2TextField.text!)\(otp3TextField.text!)\(otp4TextField.text!)\(otp5TextField.text!)"
        self.strtOTP = codeField
        print(strtOTP)
        return true
    }
    
    //MARK:-ACTION
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
           
       }
       
    //ADD CODE IN TEXTFIELD
    @IBAction func codeButtonPressed(_ sender: UIButton) {
        //MARK:- RESEND CODE PHONE AUTHETICATION
        self.otp0TextField.text = nil
        self.otp1TextField.text = nil
        self.otp2TextField.text = nil
        self.otp3TextField.text = nil
        self.otp4TextField.text = nil
        self.otp5TextField.text = nil
        self.codeLabel.isHidden = true
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
            }else {
                self.VerifcationCode = verificationID!
            }
        }
        
    }
     //VERIFY THE CODE WITH FIREBASE CODE
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        if signup == true {
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.VerifcationCode, verificationCode: self.strtOTP)
            self.resendcodeButton.isHidden = true
            self.codeLabel.isHidden = true
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let error = error {
                    let authError = error as NSError
                    print(authError.description)
                    self.codeLabel.isHidden = false
                    self.resendcodeButton.isHidden = false
                    
                }else {
                    self.codeLabel.isHidden = true
                    self.resendcodeButton.isHidden = true
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    // User has signed in successfully and currentUser object is valid
                    let currentUserInstance = Auth.auth().currentUser
                    print(currentUserInstance!)
                    
                }
                
            }
        }else {
            
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: self.VerifcationCode, verificationCode: self.strtOTP)
            self.resendcodeButton.isHidden = true
            self.codeLabel.isHidden = true
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let error = error {
                    let authError = error as NSError
                    print(authError.description)
                    self.codeLabel.isHidden = false
                    self.resendcodeButton.isHidden = false
                    
                }else {
                    self.codeLabel.isHidden = true
                    self.resendcodeButton.isHidden = true
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "UsersInboxVC") as! UsersInbox
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    // User has signed in successfully and currentUser object is valid
                    let currentUserInstance = Auth.auth().currentUser
                    print(currentUserInstance!)
                    
                }
                
            }
            
            
        }
        
    }
    
}
